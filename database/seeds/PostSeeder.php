<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;

/**
 * Class SuppliersSeeder
 */
class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Post::class, 50)->create();
    }
}
