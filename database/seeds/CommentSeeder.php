<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;

/**
 * Class SuppliersSeeder
 */
class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Comment::class, 75)->create();
    }
}
