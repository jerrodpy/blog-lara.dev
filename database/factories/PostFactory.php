<?php

use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'alias' => $faker->slug,
        'text_post' => $faker->text(500),
        'created_by' => User::all()->random()->id,
        'created_at' => Carbon::now(),
    ];
});
