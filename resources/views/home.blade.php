@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if (Session::has('message'))
                    <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
                @endif
            </div>

            <div class="col-md-12">
                <h1>Статьи</h1>
                <div>
                    <div id="list_post">
                        @foreach($posts as $post)
                            <div class="item_post">
                                <div class="row">
                                    <p class="head_post col-md-10 text-center">
                                        <a href="{!! route('show_post', [$post->alias]) !!}">{{ $post->title }}</a>
                                    </p>
                                </div>
                                <div class="row">
                                    <p class="date_post col-md-10 text-right">{{ $post->author->name }}
                                        ( {{ $post->created_at }} )</p>
                                </div>
                                <div class="row">
                                    <div class="text_post col-md-10 text-justify">
                                        {{ $post->text_post }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $posts->links() }}

                    <button type="submit" class="clickMe">Показать больше</button>
                </div>

                @if(Auth::check())
                    <h3>Добавить статью</h3>

                    <form class="form-inline" action="{{ route('create_post') }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" class="col-md-10 form-control" name="title" placeholder="Заголовок">
                        <textarea class="col-md-10 form-control" name="text_post" placeholder="Текст"></textarea>
                        <button type="submit" class="col-md-3 btn btn-outline-secondary">Добавить</button>
                    </form>
                @else
                    <div>Чтобы создавать посты необходимо авторизироватся</div>
                @endif


            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {

            $(".clickMe").click(function () {
                $.ajax({
                    dataType: 'json',
                    type: 'GET',
                    url: "{{ route('more_posts') }}",
                }).done(function (data) {

                    for (i = 0; i < data.data.length; i++) {

                        let post = data.data[i];
                        let rows = `
<div class="item_post">
    <div class="row">
        <p class="head_post col-md-10 text-center">
            <a href="/post/${post.alias}">${post.title}</a>
        </p>
        </div>
        <div class="row">
            <p class="date_post col-md-10 text-right">${post.created_at}</p>
        </div>
        <div class="row">
            <div class="text_post col-md-10 text-justify">
               ${post.text_post}
        </div>
    </div>
</div>`;

                        $("#list_post").append(rows);

                        // console.log(rows);
                    }
                });
            });
        });

    </script>
@endsection
