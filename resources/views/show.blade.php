@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if (Session::has('message'))
                <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
            @endif

            <div class="col-md-12">
                <h1>{{ $post->title }}</h1>
                <p class="date_post text-right">{{ $post->created_at }}</p>

                <div class="row">
                    <div class="text_post col-md-10 text-justify">
                        {{ $post->text_post }}

                    </div>
                </div>

                @can('update-post', $post)

                    <h2>Редактирование статьи:</h2>
                    <form class="form-inline" action="{{ route('update_post', [$post->alias]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" class="col-md-10 form-control" name="title" placeholder="Заголовок"
                               value="{{ $post->title }}">
                        <textarea class="col-md-10 form-control" name="text_post"
                                  placeholder="Текст">{{ $post->text_post }}</textarea>
                        <button type="submit" class="col-md-3 btn btn-outline-secondary">Сохранить</button>
                    </form>

                    <form class="form-inline" action="{{ route('destroy_post', [$post->alias])}}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="col-md-3 btn btn-outline-secondary">Удалить статью</button>
                    </form>
                @else
                    <p>Вы не имеете прав на редактирования Статьи</p>
                @endcan


                <h3>Комментарии:</h3>

                @foreach($post->comments as $comment)

                    <div class="item_post">
                        <div class="row">
                            <p class="head_post col-md-10 text-left">
                                {{ $comment->author->name }} ( {{ $comment->created_at }} )
                            </p>
                        </div>
                        <div class="row">
                            <div class="text_post col-md-6 text-justify">
                                {{ $comment->comment }}
                            </div>
                        </div>

                    </div>
                @endforeach

                @if(Auth::check())


                    <p>Добавить новый коментарий</p>

                    <form class="form-inline" action="{{ route('create_comment', [$post->alias]) }}" method="post">
                        {{ csrf_field() }}
                        <textarea class="col-md-10 form-control" name="comment" placeholder="Веедите текст нового коментария"></textarea>
                        <button type="submit" class="col-md-3 btn btn-outline-secondary">Создать</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
@endsection
