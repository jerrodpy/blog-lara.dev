<?php
declare( strict_types = 1 );

namespace App\Repositories\Eloquent;

use App\Models\Post;
use App\Repositories\Interfaces\PostRepositoryInterface;
use Auth;

/**
 * Class PostRepository
 * @package App\Repositories\Eloquent
 */
class PostRepository extends AbstractRepo implements PostRepositoryInterface
{
    /**
     * SocialRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Post::class);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model::create($data);
    }

    /**
     * @param array $data
     * @param Post $post
     * @return Post
     */
    public function update(array $data, Post $post): Post
    {
        $post->update($data);

        return $post;
    }

    /**
     * @param Post $post
     * @return bool
     * @throws \Exception
     */
    public function delete(Post $post): bool
    {
        return $post->delete();
    }

    /**
     * @param array $data
     * @param Post $post
     */
    public function addCommentForPost(array $data, Post $post)
    {
        $post->comments()->create($data);
    }
}
