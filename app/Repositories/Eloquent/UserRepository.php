<?php
declare( strict_types = 1 );

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Collection;

/**
 * Class UserRepository
 * @package App\Repositories\Eloquent
 */
class UserRepository extends AbstractRepo implements UserRepositoryInterface
{
    /**
     * @var User
     */
    protected $model;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(User::class);
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function getByEmail(string $email)
    {
        return $this->model::where('email', '=', $email)
                           ->first();
    }

    /**
     * @param string $column
     * @param array  $values
     * @return Collection
     */
    public function getByColumn(string $column, array $values) : Collection
    {
        return $this->model::whereIn($column, $values)
                           ->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) : User
    {
        return $this->model::create($data);
    }

    /**
     * @param array $data
     * @param User  $user
     * @return User
     */
    public function update(array $data, User $user) : User
    {
        $user->update($data);

        return $user;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function delete(User $user) : bool
    {
        return $user->delete();
    }
}
