<?php
declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Models\Comment;
use App\Repositories\Interfaces\CommentRepositoryInterface;

/**
 * Class SocialRepository
 * @package App\Repositories\Eloquent
 */
class CommentRepository extends AbstractRepo implements CommentRepositoryInterface
{
    /**
     * SocialRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Comment::class);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model::create($data);
    }

    /**
     * @param array $data
     * @param Comment $comment
     * @return Comment
     */
    public function update(array $data, Comment $comment): Comment
    {
        $comment->update($data);

        return $comment;
    }

    /**
     * @param Comment $comment
     * @return bool
     * @throws \Exception
     */
    public function delete(Comment $comment): bool
    {
        return $comment->delete();
    }
}
