<?php
declare( strict_types = 1 );

namespace App\Repositories\Interfaces;

use App\Models\Post;

/**
 * Interface PostRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface PostRepositoryInterface extends AbstractRepoInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param Post $post
     * @return Post
     */
    public function update(array $data, Post $post) : Post;

    /**
     * @param Post $post
     * @return bool
     */
    public function delete(Post $post) : bool;
}
