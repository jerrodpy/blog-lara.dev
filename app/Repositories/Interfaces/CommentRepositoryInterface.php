<?php
declare( strict_types = 1 );

namespace App\Repositories\Interfaces;

use App\Models\Comment;

/**
 * Interface CommentRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface CommentRepositoryInterface extends AbstractRepoInterface
{

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param Comment $comment
     * @return Comment
     */
    public function update(array $data, Comment $comment) : Comment;

    /**
     * @param Comment $post
     * @return bool
     */
    public function delete(Comment $post) : bool;
}
