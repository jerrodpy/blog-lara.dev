<?php
declare( strict_types = 1 );

namespace App\Repositories\Interfaces;

use App\Models\User;
use Illuminate\Support\Collection;

/**
 * Interface UserRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface UserRepositoryInterface extends AbstractRepoInterface
{
    /**
     * @param string $email
     * @return mixed
     */
    public function getByEmail(string $email);

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) : User;

    /**
     * @param array $data
     * @param User  $user
     * @return User
     */
    public function update(array $data, User $user) : User;

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user) : bool;

    /**
     * @param string $column
     * @param array  $values
     * @return Collection
     */
    public function getByColumn(string $column, array $values) : Collection;
}
