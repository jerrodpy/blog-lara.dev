<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Repositories\Eloquent\PostRepository;
use Auth;
use Illuminate\Support\Str;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * Create a new controller instance.
     *
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        return view('show', compact('post'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function morePosts()
    {
        $data = collect($this->postRepository->paginate(20));

        return response()->json($data);
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(PostRequest $request)
    {
        $data = $request->validated();
        $data['alias'] = Str::slug($data['title']);
        $data['created_by'] = Auth::user()->id;

        $this->postRepository->create($data);

        return redirect()->back()->with('message', 'Статья успешно добавлена.');
    }

    /**
     * @param PostRequest $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PostRequest $request, Post $post)
    {
        $this->postRepository->update($request->validated(), $post);

        return redirect()->back()->with('message', 'Статья успешно обновлена.');
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $this->postRepository->delete($post);

        return redirect('/')->with('message', 'Статья удалена.');
    }

    /**
     * @param CommentRequest $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addCommentForPost(CommentRequest $request, Post $post)
    {
        $data = $request->validated();
        $data['created_by'] = Auth::user()->id;

        $this->postRepository->addCommentForPost($data, $post);

        return redirect()->back()->with('message', 'Коментарий сохранен.');
    }
}
