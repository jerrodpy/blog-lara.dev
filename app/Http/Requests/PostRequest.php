<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PostRequest
 * @package App\Http\Requests
 */
class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|required|max:255',
            'text_post' => 'bail|required|max:65000',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title' => 'Заговолок должен содержать не более 255 символов',
            'text_post' => 'Текст статьи должен содержать не более 65000 символов'
        ];
    }
}
