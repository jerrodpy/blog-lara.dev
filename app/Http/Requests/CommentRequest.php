<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CommentRequest
 * @package App\Http\Requests
 */
class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'bail|required|max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'comment' => 'Текст коментария должен содержать не более 500 символов'
        ];
    }
}
