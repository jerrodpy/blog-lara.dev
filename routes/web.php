<?php
declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('/morePosts', 'PostController@morePosts')
    ->name('more_posts');

Route::post('/post/create', 'PostController@create')
    ->name('create_post')
    ->middleware('auth');

Route::get('/post/{alias}', 'PostController@show')
    ->name('show_post');


Route::post('/post/{alias}/update', 'PostController@update')
    ->name('update_post')
    ->middleware('auth');

Route::post('/post/{alias}/delete', 'PostController@destroy')
    ->name('destroy_post');


Route::post('/post/{alias}/add-comment', 'PostController@addCommentForPost')
    ->name('create_comment')
    ->middleware('auth');
